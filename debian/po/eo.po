# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to Esperanto.
# Copyright (C) 2005-2011 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Samuel Gimeno <sgimeno@gmail.com>, 2005.
# Serge Leblanc <serge.leblanc@wanadoo.fr>, 2005, 2006, 2007.
# Felipe Castro <fefcas@gmail.com>, 2008, 2009, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: lvmcfg@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:05+0000\n"
"PO-Revision-Date: 2011-03-16 20:46-0300\n"
"Last-Translator: Felipe Castro <fefcas@gmail.com>\n"
"Language-Team: Esperanto <debian-l10n-esperanto@lists.debian.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../lvmcfg.templates:1001
msgid "Configure the Logical Volume Manager"
msgstr "Agordi la Logikan Datumportil-Administrilon (LVM)"

#. Type: boolean
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:1001
msgid "Activate existing volume groups?"
msgstr "Ĉu aktivigi ekzistantajn datumportilajn grupojn?"

#. Type: boolean
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:1001
msgid ""
"${COUNT} existing volume groups have been found. Please indicate whether you "
"want to activate them."
msgstr ""
"${COUNT} ekzistantaj datumportilaj grupoj estis trovitaj. Bonvolu indiki ĉu "
"vi volas aktivigi ilin."

#. Type: select
#. Choices
#. :sl3:
#. LVM main menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:2001
msgid "Modify volume groups (VG)"
msgstr "Modifi datumportilajn grupojn (VG)"

#. Type: select
#. Choices
#. :sl3:
#. LVM main menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:2001
msgid "Modify logical volumes (LV)"
msgstr "Modifi logikajn datumportilojn (LV)"

#. Type: select
#. Choices
#. :sl3:
#. LVM main menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. Type: select
#. Choices
#. :sl3:
#. Volume groups configuration menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. Type: select
#. Choices
#. :sl3:
#. Logical volumes configuration menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:2001 ../lvmcfg-utils.templates:3001
#: ../lvmcfg-utils.templates:4001
msgid "Leave"
msgstr "Forlasi"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:2002
msgid "LVM configuration action:"
msgstr "Agorda ago de LVM (logika datumportila administrilo):"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:2002
msgid "This is the Logical Volume Manager configuration menu."
msgstr "Tio ĉi estas la agord-menuo de la Logika Datumportila Administrilo."

#. Type: select
#. Choices
#. :sl3:
#. Volume groups configuration menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:3001
msgid "Create volume groups"
msgstr "Krei datumportilajn grupojn"

#. Type: select
#. Choices
#. :sl3:
#. Volume groups configuration menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:3001
msgid "Delete volume groups"
msgstr "Forviŝi datumportilajn grupojn"

#. Type: select
#. Choices
#. :sl3:
#. Volume groups configuration menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:3001
msgid "Extend volume groups"
msgstr "Ampleksigi datumportilajn grupojn"

#. Type: select
#. Choices
#. :sl3:
#. Volume groups configuration menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:3001
msgid "Reduce volume groups"
msgstr "Redukti datumportilajn grupojn"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:3002
msgid "Volume groups configuration action:"
msgstr "Agorda ago de datumportilaj grupoj:"

#. Type: select
#. Choices
#. :sl3:
#. Logical volumes configuration menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:4001
msgid "Create logical volumes"
msgstr "Krei logikajn datumportilojn"

#. Type: select
#. Choices
#. :sl3:
#. Logical volumes configuration menu choices
#. Translators : please use infinitive form or the equivalent
#. in your language
#. Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#: ../lvmcfg-utils.templates:4001
msgid "Delete logical volumes"
msgstr "Forviŝi logikajn datumportilojn"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:4002
msgid "Logical volumes configuration action:"
msgstr "Agorda ago de logika datumportilo:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:5001
msgid "Devices for the new volume group:"
msgstr "Aparatoj por la nova datumportila grupo:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:5001
msgid "Please select the devices for the new volume group."
msgstr "Bonvolu elekti aparatojn por la nova datumportila grupo."

#. Type: multiselect
#. Description
#. :sl3:
#. Type: multiselect
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:5001 ../lvmcfg-utils.templates:17001
msgid "You can select one or more devices."
msgstr "Vi povas elekti unu aŭ pli aparatojn."

#. Type: string
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:6001
msgid "Volume group name:"
msgstr "Datumportila grupa nomo:"

#. Type: string
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:6001
msgid "Please enter the name you would like to use for the new volume group."
msgstr "Bonvolu tajpi la novon vi deziras uzi por la nova datumportila grupo."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:7001 ../lvmcfg-utils.templates:18001
msgid "No physical volumes selected"
msgstr "Neniu konkreta datumportilo elektite"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:7001
msgid ""
"No physical volumes were selected. The creation of a new volume group was "
"aborted."
msgstr ""
"Neniu datumportilo estas elektita. La kreado de nova datumportila grupo "
"fiaskis."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:8001
msgid "No volume group name entered"
msgstr "Neniu datumportila grupa nomo enigite"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:8001
msgid "No name for the volume group has been entered.  Please enter a name."
msgstr "Neniu datumportila grupa nomo estas indikita. Bonvolu tajpi nomon."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:9001
msgid "Volume group name already in use"
msgstr "Jam uzata datumportila grupa nomo"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:9001
msgid ""
"The selected volume group name is already in use. Please choose another name."
msgstr ""
"La elektita datumportila grupa nomo jam estas uzata. Bonvolu elekti alian "
"nomon."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:10001
msgid "Volume group name overlaps with device name"
msgstr "Datumportila grupa nomo konfliktas kun aparata nomo"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:10001
msgid ""
"The selected volume group name overlaps with an existing device name. Please "
"choose another name."
msgstr ""
"La elektita datumportila grupa nomo konfliktas kun ekzistanta aparata nomo. "
"Bonvolu elekti alian nomon."

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:11001
msgid "Volume group to delete:"
msgstr "Forviŝota datumportila grupo:"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:11001
msgid "Please select the volume group you wish to delete."
msgstr "Bonvolu elekti la datumportilan grupon, kiun vi volas forviŝi."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:12001 ../lvmcfg-utils.templates:15001
#: ../lvmcfg-utils.templates:20001 ../lvmcfg-utils.templates:25001
#: ../lvmcfg-utils.templates:26001 ../lvmcfg-utils.templates:33001
msgid "No volume group found"
msgstr "Neniu datumportila grupo trovite"

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:12001 ../lvmcfg-utils.templates:15001
#: ../lvmcfg-utils.templates:20001
msgid "No volume group has been found."
msgstr "Neniu datumportila grupo estas trovita."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:12001
msgid "The volume group may have already been deleted."
msgstr "La datumportila grupo eble jam estas forviŝita."

#. Type: boolean
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:13001
msgid "Really delete the volume group?"
msgstr "Ĉu vi vere forviŝos la datumportilan grupon?"

#. Type: boolean
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:13001
msgid "Please confirm the ${VG} volume group removal."
msgstr "Bonvolu konfirmi la forigon de la datumportila grupo ${VG}."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:14001
msgid "Error while deleting volume group"
msgstr "Eraro dum forviŝado de datumportila grupo"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:14001
msgid ""
"The selected volume group could not be deleted. One or more logical volumes "
"may currently be in use."
msgstr ""
"La elektitan datumportilan grupon ne eblis forviŝi. Unu aŭ pli logikaj "
"datumportiloj eble estas momente uzataj."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:15001
msgid "No volume group can be deleted."
msgstr "Neniun datumportilan grupon eblas forviŝi."

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:16001
msgid "Volume group to extend:"
msgstr "Ampleksigota datumportila grupo:"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:16001
msgid "Please select the volume group you wish to extend."
msgstr "Bonvolu elekti datumportilan grupon, kiun vi volas ampleksigi."

#. Type: multiselect
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:17001
msgid "Devices to add to the volume group:"
msgstr "Aldonotaj aparatoj por la datumportila grupo:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:17001
msgid "Please select the devices you wish to add to the volume group."
msgstr ""
"Bonvolu elekti la aparaton, kiun vi volas aldoni al la datumportila grupo."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:18001
msgid ""
"No physical volumes were selected. Extension of the volume group was aborted."
msgstr ""
"Neniu konkreta datumpartilo estas elektita. Ampleksigo de la datumportila "
"grupo fiaskis."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:19001
msgid "Error while extending volume group"
msgstr "Eraro dum ampleksigo de datumportila grupo"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:19001
msgid ""
"The physical volume ${PARTITION} could not be added to the selected volume "
"group."
msgstr ""
"La konkreta datumportilo ${PARTITION} ne povis esti aldonata al la elektita "
"datumportila grupo."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:20001
msgid "No volume group can be reduced."
msgstr "Neniun datumportilan grupon povas esti reduktata."

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:21001
msgid "Volume group to reduce:"
msgstr "Reduktota datumportila grupo:"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:21001
msgid "Please select the volume group you wish to reduce."
msgstr "Bonvolu elekti datumportilan grupon, kiun vi volas redukti."

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:22001
msgid "Device to remove from volume group:"
msgstr "Aparato por forigi el datumportila grupo:"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:22001
msgid "Please select the device you wish to remove from the volume group."
msgstr ""
"Bonvolu elekti la aparaton, kiun vi volas forigi el datumportila grupo."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:23001 ../lvmcfg-utils.templates:24001
msgid "Error while reducing volume group"
msgstr "Eraro dum reduktado de datumportila grupo"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:23001
msgid ""
"The selected volume group (${VG}) could not be reduced. There is only one "
"physical volume attached. Please delete the volume group instead."
msgstr ""
"La elektita datumportila grupo (${VG}) ne povis esti reduktata. Nur unu "
"datumportilo estas konektata. Anstataŭ tio, bonvole forviŝu la datumportilan "
"grupon."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:24001
msgid ""
"The physical volume ${PARTITION} could not be removed from the selected "
"volume group."
msgstr ""
"La konkreta datumportilo ${PARTITION} ne povis esti forigata el la elektita "
"datumportila grupo."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:24001 ../lvmcfg-utils.templates:32001
#: ../lvmcfg-utils.templates:37001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""
"Kontrolu '/var/log/syslog' aŭ rigardu la virtualan konzolon 4 por havi "
"detalojn."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:25001
msgid ""
"No volume groups were found for creating a new logical volume.  Please "
"create more physical volumes and volume groups."
msgstr ""
"Neniu datumportila grupo troviĝis por krei novan logikan datumportilon. "
"Bonvole kreu pli da konkretaj datumportiloj kaj datumportilaj grupoj."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:26001
msgid ""
"No free volume groups were found for creating a new logical volume. Please "
"create more physical volumes and volume groups, or reduce an existing volume "
"group."
msgstr ""
"Neniu libera datumportila grupo troviĝis por krei novan logikan "
"datumportilon. Bonvole kreu pli da konkretaj datumportiloj kaj datumportil-"
"grupoj, aŭ reduktu ekzistantan datumportilan grupon."

#. Type: string
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:27001
msgid "Logical volume name:"
msgstr "Logika datumportila nomo:"

#. Type: string
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:27001
msgid "Please enter the name you would like to use for the new logical volume."
msgstr ""
"Bonvolu tajpi la nomon kiun vi deziras uzi por la nova logika datumportilo."

#. Type: select
#. Description
#. :sl3:
#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:28001 ../lvmcfg-utils.templates:34001
msgid "Volume group:"
msgstr "Datumportil-grupo:"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:28001
msgid ""
"Please select the volume group where the new logical volume should be "
"created."
msgstr ""
"Bonvolu elekti datumportil-grupon kie la nova logika datumportilo devos esti "
"kreata."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:29001
msgid "No logical volume name entered"
msgstr "Neniu logika datumportila nomo indikite"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:29001
msgid "No name for the logical volume has been entered.  Please enter a name."
msgstr "Neniu logika datumportila nomo estis indikata. Bonvolu tajpi nomon."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:30001 ../lvmcfg-utils.templates:32001
msgid "Error while creating a new logical volume"
msgstr "Eraro dum kreado de nova logika datumportilo"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:30001
msgid ""
"The name ${LV} is already in use by another logical volume on the same "
"volume group (${VG})."
msgstr ""
"La nomo ${LV} jam estas uzata de alia logika datumportilo sur la sama "
"datumportil-grupo (${VG})."

#. Type: string
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:31001
msgid "Logical volume size:"
msgstr "Logika datumportila amplekso:"

#. Type: string
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:31001
msgid ""
"Please enter the size of the new logical volume. The size may be entered in "
"the following formats: 10K (Kilobytes), 10M (Megabytes), 10G (Gigabytes), "
"10T (Terabytes). The default unit is Megabytes."
msgstr ""
"Bonvolu tajpi la amplekson de la nova logika datumportilo. La amplekso povos "
"esti indikata tiamaniere: 10K (Kilobajtoj), 10M (Megabajtoj), 10G "
"(Gigabajtoj), 10T (Terabajtoj). Megabajto estas la implicita mezurunuo."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:32001
msgid ""
"Unable to create a new logical volume (${LV}) on ${VG} with the new size "
"${SIZE}."
msgstr ""
"Ne eblas krei novan logikan datumportilon (${LV}) sur ${VG} kun nova "
"amplekso ${SIZE}."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:33001
msgid "No volume group has been found for deleting a logical volume."
msgstr ""
"Neniu datumportil-grupo estis trovata por forviŝo de logika datumportilo."

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:34001
msgid ""
"Please select the volume group which contains the logical volume to be "
"deleted."
msgstr ""
"Bonvolu elekti datumportil-grupon kiu enhavas la forviŝotan logikan "
"datumportilon."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:35001
msgid "No logical volume found"
msgstr "Neniu logika datumportilo trovite"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:35001
msgid ""
"No logical volume has been found.  Please create a logical volume first."
msgstr ""
"Neniu logika datumportilo estis trovata. Bonvolu unue krei logikan "
"datumportilon."

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:36001
msgid "Logical volume:"
msgstr "Logika datumportilo:"

#. Type: select
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:36001
msgid "Please select the logical volume to be deleted on ${VG}."
msgstr ""
"Bonvolu elekti la logikan datumportilon, kiun vi volas forviŝi el ${VG}."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:37001
msgid "Error while deleting the logical volume"
msgstr "Eraro dum forviŝo de logika datumportilo"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:37001
msgid "The logical volume (${LV}) on ${VG} could not be deleted."
msgstr "La logika datumportilo (${LV}) sur ${VG} ne povis esti forviŝata."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:38001
msgid "No usable physical volumes found"
msgstr "Neniu uzebla konkreta datumportilo trovite"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:38001
msgid ""
"No physical volumes (i.e. partitions) were found in your system. All "
"physical volumes may already be in use. You may also need to load some "
"required kernel modules or re-partition the hard drives."
msgstr ""
"Neniu konkreta datumportilo (t.e. diskparto) estis trovata en via sistemo. "
"Ĉiuj konkretaj datumportiloj eble jam estas uzataj. Krome, eble vi bezonos "
"ŝargi je kelkaj necesaj kernajn modulojn aŭ rediskpartigi la fiksajn diskojn."

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:39001
msgid "Logical Volume Manager not available"
msgstr "Logika Vatumportila Administrilo ne disponeblas"

#. Type: error
#. Description
#. :sl3:
#: ../lvmcfg-utils.templates:39001
msgid ""
"The current kernel doesn't support the Logical Volume Manager. You may need "
"to load the lvm-mod module."
msgstr ""
"La nuntempa kerno ne subtenas la Logikan Datumportilan Administrilon. Eble "
"vi bezonos ŝargi la modulon lvm-mod."
